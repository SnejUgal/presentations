import "./style.scss";

import React, { useEffect } from "react";

interface Props {
  onSlideChange(step: `next` | `previous`): void;
  isAtStart: boolean;
  isAtEnd: boolean;
}

const Navigation = ({ onSlideChange, isAtStart, isAtEnd }: Props) => {
  useEffect(() => {
    const handler = (event: KeyboardEvent) => {
      if (event.key === `ArrowLeft` && !isAtStart) {
        onSlideChange(`previous`);
      } else if (event.key === `ArrowRight` && !isAtEnd) {
        onSlideChange(`next`);
      }
    };

    document.body.addEventListener(`keydown`, handler);

    return () => document.body.removeEventListener(`keydown`, handler);
  });
  return (
    <nav className="navigation">
      <button
        className="navigation_button -previous"
        onClick={() => onSlideChange(`previous`)}
        disabled={isAtStart}
      />
      <a
        href="https://gitlab.com/snejugal/presentations/-/tree/master/astronomical-models"
        className="navigation_sourceCode"
      >
        Исходный код
      </a>
      <button
        className="navigation_button -next"
        onClick={() => onSlideChange(`next`)}
        disabled={isAtEnd}
      />
    </nav>
  );
};

export default Navigation;

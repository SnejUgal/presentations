import "./style.scss";

import React, { useState } from "react";
import { SLIDES, Slide } from "../common";
import Navigation from "../Navigation";
import Text from "../Text";
import Animation from "../Animation";

const App = () => {
  const [slide, setSlide] = useState<Slide>(`title`);

  const handleSlideChange = (step: `next` | `previous`) => {
    const currentIndex = SLIDES.indexOf(slide);
    const newIndex = currentIndex + (step === `next` ? 1 : -1);

    setSlide(SLIDES[newIndex]);
  };

  return (
    <>
      <Text slide={slide} />
      <Animation slide={slide} />
      <Navigation
        onSlideChange={handleSlideChange}
        isAtStart={slide === SLIDES[0]}
        isAtEnd={slide === SLIDES[SLIDES.length - 1]}
      />
    </>
  );
};

export default App;

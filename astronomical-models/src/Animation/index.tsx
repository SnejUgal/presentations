import "./style.scss";

import React from "react";
import { Slide } from "../common";
import { ReactComponent as Anthropocentrism } from "./anthropocentrism.svg";
import { ReactComponent as AncientGreece } from "./ancientGreece.svg";
import { ReactComponent as Geocentrism } from "./geocentrism.svg";
import { ReactComponent as Movements } from "./movements.svg";
import { ReactComponent as Heliocentrism } from "./heliocentrism.svg";
import { ReactComponent as Parallax } from "./parallax.svg";

interface Props {
  slide: Slide;
}

const animations = {
  title: () => null,
  anthropocentrism: Anthropocentrism,
  ancientGreece: AncientGreece,
  geocentrism: Geocentrism,
  movements: Movements,
  heliocentrism: Heliocentrism,
  parallax: Parallax,
  final: () => null,
};

const Animation = ({ slide }: Props) => {
  const SlideAnimation = animations[slide];

  return (
    <article className="animation">
      <SlideAnimation />
    </article>
  );
};

export default Animation;

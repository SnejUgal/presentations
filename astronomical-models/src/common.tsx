export const SLIDES = [
  `title` as const,
  `anthropocentrism` as const,
  `ancientGreece` as const,
  `geocentrism` as const,
  `movements` as const,
  `heliocentrism` as const,
  `parallax` as const,
  `final` as const,
];

export type Slide = (typeof SLIDES)[number];

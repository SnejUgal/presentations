import "./style.scss";

import React, { useState } from "react";
import Stars from "../Stars";
import Footer, { Tab } from "../Footer";

const App = () => {
  const [activeTab, setActiveTab] = useState<Tab>(null);

  return (
    <>
      <Stars onTabSelect={setActiveTab} />
      <Footer activeTab={activeTab} onTabSelect={setActiveTab} />
    </>
  );
};

export default App;

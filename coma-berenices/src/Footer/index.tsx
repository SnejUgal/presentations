import "./style.scss";

import React from "react";
import classNames from "classnames";

const tabNames = {
  description: `Описание`,
  name: `Название`,
  alpha: `α Волос Вероники`,
  beta: `β Волос Вероники`,
  gamma: `γ Волос Вероники`,
};

const tabDescriptions = {
  description: (
    <>
      <p>
        Волосы Вероники&nbsp;&mdash; созвездие северного полушария неба.
        Содержит 64&nbsp;звезды, которые можно увидеть невооруженным глазом,
        но&nbsp;ярких звёзд нет. В&nbsp;этом созвездии видны тысячи галактик
        и&nbsp;сотни их&nbsp;скоплений.
      </p>
      <p>
        Созвездие лучше всего видно в&nbsp;марте&mdash;апреле. Оно расположено
        между α&nbsp;Волопаса и&nbsp;β&nbsp;Льва. В&nbsp;безлунную ночь
        в&nbsp;этом месте можно увидеть группу слабых звёзд&nbsp;&mdash;
        созвездие Волосы Вероники.
      </p>
    </>
  ),
  name: (
    <>
      <p>
        По&nbsp;преданию, это&nbsp;созвездие своим названием обязано
        Беренике&nbsp;&mdash; жене египетского царя Птомелея&nbsp;III.
        Она&nbsp;отрезала свои прекрасные волосы и&nbsp;поместила&nbsp;их
        в&nbsp;храме Афродиты в&nbsp;благодарность богине за&nbsp;победу
        над&nbsp;сирийцами. На&nbsp;следующий день жрец-астроном сообщил царской
        чете, что&nbsp;жертва была принята, и&nbsp;ночью он&nbsp;наблюдал новые
        звёзды в&nbsp;виде женских кос.
      </p>
    </>
  ),
  alpha: (
    <>
      <p>
        Имеет звёздную величину, равную&nbsp;4,32&nbsp;&mdash; тусклее,
        чем&nbsp;β&nbsp;Волос Вероники. Является двойной звездой&nbsp;&mdash;
        системой из&nbsp;двух звёзд, которые вращаются вокруг общего центра
        масс. Имеет традиционное имя «Диамеда».
      </p>
    </>
  ),
  beta: (
    <>
      <p>
        Имеет звёздную величину, равную&nbsp;4,26&nbsp;&mdash; ярче,
        чем&nbsp;α&nbsp;Волос Вероники. Находится в&nbsp;30&nbsp;световых годах
        от&nbsp;Солнца.
      </p>
    </>
  ),
  gamma: (
    <>
      <p>
        Имеет звёздную величину, равную&nbsp;4,36. Находится на&nbsp;расстоянии
        167&nbsp;световых лет от&nbsp;Солнца.
      </p>
    </>
  ),
};

export type Tab = keyof typeof tabNames | null;

interface DescriptionProps {
  tab: keyof typeof tabNames;
}

const Description = ({ tab }: DescriptionProps) => {
  return (
    <div className="description">
      <div className="description_inner">
        <h2 className="description_title">{tabNames[tab]}</h2>
        {tabDescriptions[tab]}
      </div>
    </div>
  );
};

interface TabProps {
  tab: keyof typeof tabNames;
  isActive: boolean;
  onSelect(tab: Tab): void;
}

const Tab = ({ tab, isActive, onSelect }: TabProps) => {
  const className = classNames(`tabs_tab`, {
    "-isActive": isActive,
  });

  return (
    <li>
      <button
        className={className}
        onClick={() => onSelect(isActive ? null : tab)}
      >
        {tabNames[tab]}
      </button>
    </li>
  );
};

interface FooterProps {
  activeTab: Tab;
  onTabSelect(tab: Tab): void;
}

const Footer = ({ activeTab, onTabSelect }: FooterProps) => {
  return (
    <footer className="footer">
      <h1 className="footer_constellation">Волосы Вероники</h1>
      <ul className="tabs">
        <Tab
          tab="description"
          isActive={activeTab === `description`}
          onSelect={onTabSelect}
        />
        <Tab
          tab="name"
          isActive={activeTab === `name`}
          onSelect={onTabSelect}
        />
        {activeTab === null || [`description`, `name`].includes(activeTab) || (
          <Tab tab={activeTab} isActive={true} onSelect={onTabSelect} />
        )}
      </ul>
      <a
        className="footer_sourceCode"
        href="https://gitlab.com/SnejUgal/presentations/-/tree/master/coma-berenices"
      >
        Исходный код
      </a>
      {activeTab !== null && <Description tab={activeTab} />}
    </footer>
  );
};

export default Footer;
